# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby File.read(".ruby-version").strip

# Framework base
gem "rails"

# Databases / Jobs
gem "pg"
gem "redis"
gem "sidekiq"

# Web server
gem "puma"

# Framework extensions
gem "light-service"
gem "jbuilder"
gem "responders", github: "heartcombo/responders", branch: "main"

# Assets
gem "cssbundling-rails"
gem "importmap-rails"
gem "jsbundling-rails"
gem "sprockets-rails"
gem "stimulus-rails"
gem "turbo-rails"

# helpers
gem "acts_as_favoritor"
gem "font-awesome-sass"
gem "friendly_id"
gem "name_of_person"
gem "noticed"
gem "sitemap_generator"
gem "smarter_csv", git: "https://github.com/tilo/smarter_csv.git", branch: "2.0-develop"
gem "searchkick"
gem "opensearch-ruby"

# Auth
gem "devise"
gem "omniauth-facebook"
gem "omniauth-github"
gem "omniauth-twitter"
gem "pundit"

# Admin
gem "madmin"
gem "pretender"

# Framework helper
gem "bootsnap", require: false
gem "image_processing"
gem "tzinfo-data", platforms: %i[ mingw mswin x64_mingw jruby ]
gem "whenever", require: false

## Development and Test ##
group :development, :test do
  gem "pry-byebug"
  gem "factory_bot_rails"
  gem "faker"
  gem "rspec-rails"
end

group :development do
  gem "guard-bundler"
  gem "guard-rails"
  gem "guard-rspec"
  gem "guard-rubocop"
  gem "guard-livereload"
  gem "rubocop-rails"
  gem "rubocop-rails_config"
  gem "terminal-notifier-guard"
  gem "web-console"
end

group :test do
  gem "capybara"
  gem "shoulda-matchers"
  gem "selenium-webdriver"
  gem "vcr"
  gem "webdrivers"
  gem "webmock", require: "webmock/rspec"
end
