# frozen_string_literal: true

class AddSocialReasonReferenceToOrganizations < ActiveRecord::Migration[7.0]
  def change
    add_belongs_to :organizations, :primary_social_reason, null: false, foreign_key: false
    add_belongs_to :organizations, :secondary_social_reason, null: true, foreign_key: false
  end
end
