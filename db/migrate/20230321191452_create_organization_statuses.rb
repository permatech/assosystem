# frozen_string_literal: true

class CreateOrganizationStatuses < ActiveRecord::Migration[7.0]
  def change
    create_table :organization_statuses do |t|
      t.integer :status, default: 0
      t.belongs_to :organization, null: false, foreign_key: true

      t.timestamps
    end
  end
end
