# frozen_string_literal: true

class CreateDataFiles < ActiveRecord::Migration[7.0]
  def change
    create_table :data_files do |t|
      t.string :original_filename
      t.text :content

      t.timestamps
    end
  end
end
