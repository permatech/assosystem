# frozen_string_literal: true

class CreateOrganizations < ActiveRecord::Migration[7.0]
  def change
    create_table :organizations do |t|
      t.string :national_uid
      t.string :former_uid
      t.string :siret
      t.string :site_administrator_code
      t.date :creation_date
      t.date :declaration_date
      t.date :publication_date
      t.date :dissolution_date
      t.string :kind_code
      t.string :grouping
      t.string :title
      t.string :short_title
      t.string :reason
      t.integer :social_reason_1
      t.integer :social_reason_2
      t.string :additional_address
      t.string :street_number
      t.string :additional_street_number
      t.string :street_type
      t.string :street_name
      t.string :additional_street_name
      t.string :insee_address_code
      t.string :zipcode
      t.string :municipality_description
      t.string :declarant_family_name
      t.string :manager_address
      t.string :manager_additional_address
      t.string :manager_street_name
      t.string :manager_additional_street_name
      t.string :manager_zipcode
      t.string :manager_billing_address
      t.string :manager_state
      t.string :manager_gender
      t.string :web_site
      t.boolean :web_publication
      t.string :remark
      t.string :status
      t.datetime :data_update_time

      t.belongs_to :data_file, null: false, foreign_key: true

      t.timestamps
    end
    add_index :organizations, :national_uid, unique: true
    add_index :organizations, :siret, unique: true
  end
end
