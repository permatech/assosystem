# frozen_string_literal: true

class AddResourceToDataFile < ActiveRecord::Migration[7.0]
  def change
    add_column :data_files, :data_category, :integer
  end
end
