# frozen_string_literal: true

class CreateSocialReasons < ActiveRecord::Migration[7.0]
  def change
    create_table :social_reasons do |t|
      t.integer :parent_code
      t.integer :code
      t.string :description
      t.belongs_to :data_file, null: false, foreign_key: true

      t.timestamps
    end
  end
end
