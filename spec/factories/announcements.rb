# frozen_string_literal: true

FactoryBot.define do
  factory :announcement do
    id { 1 }
    published_at { "2023-02-16 10:29:20" }
    announcement_type { "MyString" }
    name { "MyString" }
    description { "MyText" }
    created_at { "2023-02-16 10:29:20" }
    updated_at { "2023-02-16 10:29:20" }
  end
end
