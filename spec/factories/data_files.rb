# frozen_string_literal: true

FactoryBot.define do
  factory :data_file do
    original_filename { "/path/to/filename" }
    content {
      "header_1, header_2
      value_1, value_2"
    }
  end
end
