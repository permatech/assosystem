# frozen_string_literal: true

FactoryBot.define do
  factory :social_reason do
    parent_code { Faker::Number.number(digits: 4) }
    code { Faker::Number.unique.number(digits: 4) }
    description { Faker::Lorem.sentence }

    association :data_file

    trait :for_rna_row_1 do
      parent_code { 6000 }
      code { 6090 }
    end

    trait :for_rna_row_2 do
      parent_code { 9000 }
      code { 9040 }
    end

    trait :with_code_9000 do
      parent_code { 9000 }
      code { 9000 }
      description { "ACTION SOCIOCULTURELLE" }
    end

    trait :with_code_9005 do
      parent_code { 9000 }
      code { 9005 }
      description { "maisons de jeunes, foyers, clubs de jeunes" }
    end

    trait :with_code_9007 do
      parent_code { 9000 }
      code { 9007 }
      description { "maisons de la culture, office municipal,centres culturels" }
    end
  end

  trait :with_code_30005 do
    parent_code { 30000 }
    code { 30005 }
    description { "comité, défense d'un emploi" }
  end

  trait :with_code_30010 do
    parent_code { 30000 }
    code { 30010 }
    description { "entreprises d'insertion, associations intermédiaires, régies de quartier" }
  end

  trait :with_code_1005 do
    parent_code { 1000 }
    code { 1005 }
    description { "associations à caractère politique général" }
  end
end
