# frozen_string_literal: true

FactoryBot.define do
  factory :organization_status do
    active
    association :organization

    traits_for_enum :status
  end
end
