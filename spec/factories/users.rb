# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    admin { false }

    trait :favoritor do
      after(:create) do |user|
        user.favorite(factory :organization)
      end
    end

    trait :admin do
      admin { true }
    end
  end
end
