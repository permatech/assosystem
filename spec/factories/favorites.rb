# frozen_string_literal: true

FactoryBot.define do
  factory :favorite do
    for_organization

    favoritor factory: :user
    notes { Faker::Markdown.headers }

    trait :for_organization do
      favoritable factory: :organization
    end

    trait :with_empty_notes do
      notes { "" }
    end
  end
end
