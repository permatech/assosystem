# frozen_string_literal: true

FactoryBot.define do
  factory :organization do
    active

    national_uid { "W#{Random.alphanumeric(5)}" }
    former_uid { "MyString" }
    siret { Faker::Company.french_siret_number }
    site_administrator_code { "MyString" }
    creation_date { "2023-02-16" }
    publication_date { "2023-02-16" }
    dissolution_date { "2023-02-16" }
    kind_code { "MyString" }
    grouping { "MyString" }
    title { Faker::Company.name }
    short_title { "MyString" }
    reason { "MyString" }
    social_reason_1 { Faker::Number.number digits: 4 }
    social_reason_2 { Faker::Number.number digits: 4 }
    additional_address { "MyString" }
    street_number { "MyString" }
    additional_street_number { "MyString" }
    street_type { "MyString" }
    street_name { "MyString" }
    additional_street_name { "MyString" }
    insee_address_code { "MyString" }
    zipcode { "MyString" }
    municipality_description { "MyString" }
    declarant_family_name { "MyString" }
    manager_address { "MyString" }
    manager_additional_address { "MyString" }
    manager_street_name { "MyString" }
    manager_additional_street_name { "MyString" }
    manager_zipcode { "MyString" }
    manager_billing_address { "MyString" }
    manager_state { "MyString" }
    manager_gender { "MyString" }
    web_site { "MyString" }
    web_publication { false }
    remark { "MyString" }
    status { "MyString" }
    data_update_time { "2023-02-16 22:01:31" }

    association :data_file
    association :primary_social_reason, factory: :social_reason
    association :secondary_social_reason, factory: :social_reason


    trait :searchable do
      after(:create) do |organization, _evaluator|
        organization.reindex(refresh: true)
      end
    end

    trait :with_primary_social_reason_9005 do
      association :primary_social_reason, :with_code_9005, factory: :social_reason
    end

    trait :with_primary_social_reason_9007 do
      association :primary_social_reason, :with_code_9007, factory: :social_reason
    end

    trait :with_primary_social_reason_30005 do
      association :primary_social_reason, :with_code_30005, factory: :social_reason
    end

    trait :with_primary_social_reason_30010 do
      association :primary_social_reason, :with_code_30010, factory: :social_reason
    end

    trait :with_primary_social_reason_30005_and_secondary_9005 do
      association :primary_social_reason, :with_code_30005, factory: :social_reason
      association :secondary_social_reason, :with_code_9005, factory: :social_reason
    end

    trait :with_primary_social_reason_30005_and_secondary_1005 do
      association :primary_social_reason, :with_code_30005, factory: :social_reason
      association :secondary_social_reason, :with_code_1005, factory: :social_reason
    end

    trait :active do
      after(:create) do |organization|
        create :organization_status, :active, organization:
      end
    end

    trait :dissolved do
      after(:create) do |organization|
        organization.administrative_status.dissolved!
      end
    end

    trait :canceled do
      after(:create) do |organization|
        organization.administrative_status.canceled!
      end
    end
  end
end
