# frozen_string_literal: true

require "rails_helper"

RSpec.describe DataFiles::ImportResourcesFromCsvFile, type: :organizer do
  context "for organizations" do
    before do
      create :social_reason, :for_rna_row_1
      create :social_reason, :for_rna_row_2
    end

    context "with valid context" do
      let(:context) do
        {
          data_category: "0",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_2_rows.csv")
        }
      end

      subject { described_class.call(context) }

      it "executes the actions in order" do
        expect(DataFiles::ExtractDataFromFile).to receive(:execute).ordered.and_call_original
        expect(DataFiles::BackUpRawData).to receive(:execute).ordered.and_call_original
        expect(DataFiles::CompareOrganizationData).to receive(:execute).ordered.and_call_original
        expect(DataFiles::CreateOrganizationsFromData).to receive(:execute).ordered.and_call_original
        expect(DataFiles::UpdateOrganizationsFromData).to receive(:execute).ordered.and_call_original
        subject
      end
      it { expect(subject).to be_success }
    end

    context "with some duplicated data" do
      let(:duplicated_context) do
        {
          data_category: "0",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_duplicated_uid.csv")
        }
      end

      subject { described_class.call duplicated_context }

      it { expect(subject).to be_success }
      it { expect(subject.extracted_data.count).to eq(3) }
      it { expect { subject }.to change(DataFile, :count).by(1) }
      it { expect { subject }.to change(Organization, :count).by(2) }
      it { expect(subject.message).to eq nil }
    end

    context "with unchanged Organizations" do
      before do
        create :organization, national_uid: "W9M1000001", data_update_time: "2008-08-11 23:43:32"
        create :organization, national_uid: "W9M1000002", data_update_time: "2007-04-03 18:40:20"
      end
      let(:duplicated_context) do
        {
          data_category: "0",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_duplicated_uid.csv")
        }
      end

      subject { described_class.call duplicated_context }

      it { expect(subject).to be_failure }
      it { expect(subject.extracted_data.count).to eq(3) }
      it { expect { subject }.to change(DataFile, :count).by(0) }
      it { expect { subject }.to change(Organization, :count).by(0) }
      it { expect(subject.message).to eq "No Organizations were created nor updated." }
    end
  end

  context "for social reasons" do
    context "with valid context" do
      let(:context) do
        {
          data_category: "1",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_social_reasons_30_rows.csv")
        }
      end

      subject { described_class.call(context) }

      it "executes the actions in order" do
        expect(DataFiles::ExtractDataFromFile).to receive(:execute).ordered.and_call_original
        expect(DataFiles::BackUpRawData).to receive(:execute).ordered.and_call_original
        expect(DataFiles::CompareSocialReasonData).to receive(:execute).ordered.and_call_original
        expect(DataFiles::CreateSocialReasonsFromData).to receive(:execute).ordered.and_call_original
        expect(DataFiles::UpdateSocialReasonsFromData).to receive(:execute).ordered.and_call_original
        subject
      end
      it { expect(subject).to be_success }
    end

    context "with duplicated data" do
      let(:duplicated_context) do
        {
          data_category: "1",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_social_reasons_duplicated_code.csv")
        }
      end

      subject { described_class.call duplicated_context }

      it { expect(subject).to be_success }
      it { expect(subject.extracted_data.count).to eq(3) }
      it { expect { subject }.to change(DataFile, :count).by(1) }
      it { expect { subject }.to change(SocialReason, :count).by(2) }
      it { expect(subject.message).to eq nil }
    end

    context "with unchanged Social Reasons" do
      before do
        create :social_reason, :with_code_9000
        create :social_reason, :with_code_9005
      end
      let(:duplicated_context) do
        {
          data_category: "1",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_social_reasons_duplicated_code.csv")
        }
      end

      subject { described_class.call duplicated_context }

      it { expect(subject).to be_failure }
      it { expect(subject.extracted_data.count).to eq(3) }
      it { expect { subject }.to change(DataFile, :count).by(0) }
      it { expect { subject }.to change(SocialReason, :count).by(0) }
      it { expect(subject.message).to eq "No Social Reasons were created nor updated." }
    end
  end
end
