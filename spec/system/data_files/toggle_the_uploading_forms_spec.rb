# frozen_string_literal: true

require "rails_helper"

RSpec.describe "DataFiles::ToggleTheUploadingForms", type: :system do
  before do
    driven_by(:rack_test)
    admin_user = create :user, :admin
    sign_in admin_user
  end

  scenario "I can toggle the uploading form" do
    visit "/data_files"
    expect(page).not_to have_selector("form", id: "new_data_file")

    click_on "Upload Data File"
    expect(page).to have_selector("form", id: "new_data_file")

    click_on "Cancel"
    expect(page).not_to have_selector("form", id: "new_data_file")
  end
end
