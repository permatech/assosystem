# frozen_string_literal: true

require "rails_helper"

RSpec.describe "DataFiles::NavigateToTheDataFilesPages", type: :system do
  before do
    driven_by(:rack_test)
  end

  context "As admin User" do
    before do
      user = create :user, :admin
      sign_in user
    end

    scenario "I can navigate back and forth to the Data Files page" do
      visit root_path

      click_link "Data Files"

      expect(current_path).to eq data_files_path

      click_link "AssoSystem"

      expect(current_path).to eq root_path
    end
  end

  context "As non-admin User" do
    before do
      user = create :user
      sign_in user
    end

    scenario "I cannot see link to access the Data Files page" do
      visit root_path

      expect(page).not_to have_link "Data Files"
    end

    scenario "I cannot access to the Data Files page" do
      expect {
        visit data_files_path
      }.to raise_error(ActionController::RoutingError)
    end
  end
end
