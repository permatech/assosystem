# frozen_string_literal: true

require "rails_helper"

RSpec.describe "DataFiles::ImportResourcesFromDataFiles", type: :system do
  before do
    driven_by(:rack_test)
  end

  context "As admin User" do
    before do
      admin_user = create :user, :admin
      sign_in admin_user
    end

    context "When I upload Organizations Data File" do
      before do
        create :social_reason, :for_rna_row_1
        create :social_reason, :for_rna_row_2
      end

      scenario "I see the uploaded file name"   do
        visit data_files_path

        click_on "Upload Data File"
        select "Organizations"
        attach_file "data_file_resources_data", "spec/support/test_data_files/fake_rna_2_rows.csv"
        click_on "Upload"

        expect(current_path).to eq data_files_path
        expect(page).to have_content "fake_rna_2_rows.csv"
      end
    end

    context "When I upload Social Reasons Data File" do
      context "for the first time" do
        scenario "I see a information notice before uploading"   do
          visit data_files_path
          click_on "Upload Data File"

          expect(page).to have_content "Upload a Social Reasons Data File"

          attach_file "data_file_resources_data", "spec/support/test_data_files/fake_social_reasons_30_rows.csv"
          click_on "Upload"

          expect(current_path).to eq data_files_path

          click_on "Upload Data File"

          expect(page).not_to have_content "Upload a Social Reasons Data File"
        end
      end

      context "for next times" do
        before do
          create :social_reason
        end

        scenario "I see the uploaded file name"   do
          visit data_files_path

          click_on "Upload Data File"
          select "Social reasons"
          attach_file "data_file_resources_data", "spec/support/test_data_files/fake_social_reasons_30_rows.csv"
          click_on "Upload"

          expect(current_path).to eq data_files_path
          expect(page).to have_content "fake_social_reasons_30_rows.csv"
        end
      end
    end
  end

  context "As non-admin User" do
    before do
      user = create :user
      sign_in user
    end

    scenario "I cannot access to the data files page"   do
      expect {
        visit data_files_path
      }.to raise_error(ActionController::RoutingError)
    end
  end

  context "As unsigned User" do
    before do
      user = create :user
      sign_out user
    end

    scenario "I am redirected to log in"   do
      visit data_files_path

      expect(current_path).to eq new_user_session_path
    end
  end
end
