# frozen_string_literal: true

require "rails_helper"

RSpec.describe "FavoriteOrganizations::NavigateToAFavoriteOrganization", type: :system do
  before do
    driven_by(:rack_test)
    @user = create :user

    @favoritable_organization_1 = create :organization
    @favoritable_organization_2 = create :organization
    @user.favorite @favoritable_organization_1
    @user.favorite @favoritable_organization_2
  end

  context "When I visit my Favorite Organizations page" do
    before do
      sign_in @user
    end

    scenario "I can navigate back and forth to each of my Favorite Organizations" do
      visit favorite_organizations_path

      click_link @favoritable_organization_1.title

      expect(current_path).to eq favorite_organization_path(@favoritable_organization_1)

      within ".breadcrumb" do
        click_link "My Favorite Organizations"
      end

      expect(current_path).to eq favorite_organizations_path

      click_link @favoritable_organization_2.title

      expect(current_path).to eq favorite_organization_path(@favoritable_organization_2)

      within ".breadcrumb" do
        click_link "My Favorite Organizations"
      end

      expect(current_path).to eq favorite_organizations_path
    end
  end
end
