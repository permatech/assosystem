# frozen_string_literal: true

require "rails_helper"

RSpec.describe "FavoriteOrganizations::UsersCanAnnotateTheirFavoriteOrganizations", type: :system do
  before do
    driven_by(:selenium)
    @user = create :user
    sign_in @user
  end

  context "When I visit a Favorite Organizaiton annotated by me" do
    before do
      @annotated_favoritable = create(:favorite, favoritor: @user)
    end

    scenario "I can see the notes" do
      visit favorite_organization_path @annotated_favoritable.favoritable

      expect(page).to have_content @annotated_favoritable[:notes]
    end

    context "When I edit notes" do
      before do
        @edited_notes = "This is an edited note"
      end

      scenario "I can see the edited notes" do
        visit favorite_organization_path @annotated_favoritable.favoritable

        click_on "Edit notes"
        fill_in_rich_text_area "Notes", with: @edited_notes
        click_on "Update notes"

        expect(current_path).to eq favorite_organization_path @annotated_favoritable.favoritable
        expect(page).to have_content @edited_notes
      end
    end
  end

  context "When I visit a Favorite Organizaiton with empty notes" do
    before do
      @favoritable_organization = create(:favorite, :with_empty_notes, favoritor: @user).favoritable
    end

    scenario "I can see a placeholder" do
      visit favorite_organization_path @favoritable_organization

      expect(page).to have_content "No notes yet."
    end
  end
end
