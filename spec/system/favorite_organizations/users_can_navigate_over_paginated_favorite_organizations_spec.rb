# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Organizations::UsersCanNavigateOverPaginatedFavoriteOrganizations", type: :system do
  before do
    driven_by(:selenium)
    @user = create :user
    sign_in @user
  end

  context "When I visit the Favorite Organizations page" do
    context "with 12 Favorite Organizations" do
      before do
        @favoritable_organizations = create_list :organization, 12
        @favoritable_organizations.each do |favoritable_organization|
          @user.favorite favoritable_organization
        end
      end

      scenario "I can see the Favorite Organizations split on two pages" do
        visit favorite_organizations_path

        within ".pagination" do
          within ".page-item.active" do
            expect(page).to have_content("1")
          end

          click_on "2"
          within ".page-item.active" do
            expect(page).to have_content("2")
          end

          expect(page).not_to have_content("3")
        end
      end

      context "when I remove 2 Organization from Favorite" do
        scenario "I can't see anymore paginations" do
          visit favorite_organizations_path

          expect(page).to have_css(".pagination")

          within "#organization_#{@favoritable_organizations.first.id}" do
            click_on("Remove from favorites")
          end

          within "#organization_#{@favoritable_organizations.second.id}" do
            click_on("Remove from favorites")
          end

          expect(page).not_to have_css(".pagination")
        end
      end
    end

    context "with 5 Favorite Organizations" do
      before do
        @favoritable_organizations = create_list :organization, 5
        @favoritable_organizations.each do |favoritable_organization|
          @user.favorite favoritable_organization
        end
      end

      scenario "I can't see any paginations" do
        visit favorite_organizations_path

        expect(page).not_to have_css(".pagination")
      end
    end
  end
end
