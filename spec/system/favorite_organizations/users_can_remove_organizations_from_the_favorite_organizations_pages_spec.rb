# frozen_string_literal: true

require "rails_helper"

RSpec.describe "FavoriteOrganizations::UsersCanRemoveOrganizationsFromTheFavoriteOrganizationsPages", type: :system do
  before do
    driven_by(:selenium)
    @user = create :user
    sign_in @user

    @favoritable_organizations = create_list :organization, 3
    @favoritable_organizations.each do |favoritable_organization|
      @user.favorite favoritable_organization
    end
    @unfavoritable_organization = create :organization
    @user.favorite @unfavoritable_organization
  end

  context "When I click on remove from Favorite" do
    scenario "I can't see the unfavorited Organization anymore" do
      visit favorite_organizations_path

      @favoritable_organizations.each do |favoritable_organization|
        expect(page).to have_content(favoritable_organization.title)
      end
      expect(page).to have_content(@unfavoritable_organization.title)

      within "#organization_#{@unfavoritable_organization.id}" do
        click_on "Remove from favorites"
      end

      @favoritable_organizations.each do |favoritable_organization|
        expect(page).to have_content(favoritable_organization.title)
      end
      expect(page).not_to have_content(@unfavoritable_organization.title)
    end
  end
end
