# frozen_string_literal: true

require "rails_helper"

RSpec.describe "FavoriteOrganizations::NavigateToTheFavoriteOrganizationsPage", type: :system do
  before do
    driven_by(:rack_test)
    @user = create :user
  end

  context "As signed-in User" do
    before do
      sign_in @user
    end

    scenario "I can navigate back and forth to the favorite organizations page" do
      visit "/"

      click_link "My Favorite Organizations"

      expect(current_url).to eq favorite_organizations_url

      click_link "AssoSystem"

      expect(current_url).to eq root_url
    end
  end

  context "As unsigned User" do
    before do
      sign_out @user
    end

    scenario "I cannot see the favorite organizations link" do
      visit "/"

      expect(page).not_to have_link "My Favorite Organizations"
    end
  end
end
