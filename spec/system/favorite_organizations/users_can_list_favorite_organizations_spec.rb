# frozen_string_literal: true

require "rails_helper"

RSpec.describe "FavoriteOrganizations::UsersCanListFavoriteOrganizations", type: :system do
  before do
    driven_by(:rack_test)
    @user = create :user
    sign_in @user

    @favoritable_organizations = create_list :organization, 3
    @favoritable_organizations.each do |favoritable_organization|
      @user.favorite favoritable_organization
    end
    @other_organizations = create_list :organization, 3
    @annotated_favoritable = create(:favorite, favoritor: @user, notes: "###One two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen ninteen twenty twenty-one")
  end

  context "When I visit the Favorite Organization page" do
    scenario "I see a list of my Favorite Organizations" do
      visit "/favorite_organizations"

      @favoritable_organizations.each do |favoritable_organization|
        expect(page).to have_content favoritable_organization.title
        expect(page).to have_content favoritable_organization.reason.truncate_words(20, omission: "...")
      end

      @other_organizations.each do |other_organization|
        expect(page).not_to have_content other_organization.title
      end
      # binding.pry
      expect(page).to have_content "###One two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen ninteen twenty..."
    end

    context "With an annoted Favorite Organization and an unannoted one" do
    end
  end
end
