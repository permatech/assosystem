# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Organizations::NavigateToTheOrganizationsPage", type: :system do
  before do
    driven_by(:rack_test)
  end

  scenario "I can navigate back and forth to the organizations page" do
    visit root_url

    click_link "Organizations"

    expect(current_url).to eq organizations_url

    click_link "AssoSystem"

    expect(current_url).to eq root_url
  end
end
