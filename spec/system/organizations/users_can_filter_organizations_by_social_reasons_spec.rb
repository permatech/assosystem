# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Organizations::UsersCanFilterOrganizationsBySocialReasons", type: :system do
  before do
    driven_by(:rack_test)
  end

  context "When I select one Social Reason" do
    before do
      @matching_organization = create :organization,
      :with_primary_social_reason_9005,
      :searchable
      @secondary_matching_organization = create :organization,
      :with_primary_social_reason_30005,
      :searchable,
      secondary_social_reason: @matching_organization.primary_social_reason
      @other_organization = create :organization,
      :with_primary_social_reason_9007,
      :searchable
      Organization.reindex
    end

    scenario "I can see only the corresponding Organizations", vcr: false do
      visit "/organizations"
      select "9005 - maisons de jeunes, foyers, clubs de jeunes"
      click_on "Search"

      expect(page).to have_content(@matching_organization.title)
      expect(page).to have_content(@secondary_matching_organization.title)
      expect(page).not_to have_content(@other_organization.title)

      within "#current_filters" do
        expect(page).to have_content "9005"
      end
    end
  end

  context "When I select multiple Social Reasons" do
    before do
      social_reason_1005 = create :social_reason, :with_code_1005
      social_reason_9005 = create :social_reason, :with_code_9005
      social_reason_9007 = create :social_reason, :with_code_9007
      social_reason_30005 = create :social_reason, :with_code_30005
      social_reason_30010 = create :social_reason, :with_code_30010

      @matching_organization_1 = create :organization,
      :searchable,
      primary_social_reason: social_reason_9005,
      secondary_social_reason: social_reason_9005
      @matching_organization_2 = create :organization,
      :searchable,
      primary_social_reason: social_reason_9007,
      secondary_social_reason: social_reason_1005
      @secondary_matching_organization = create :organization,
      :searchable,
      primary_social_reason: social_reason_30005,
      secondary_social_reason: social_reason_9005

      @other_organization_1 = create :organization,
      :searchable,
      primary_social_reason: social_reason_30005,
      secondary_social_reason: social_reason_30005
      @other_organization_2 = create :organization,
      :searchable,
      primary_social_reason: social_reason_30010,
      secondary_social_reason: social_reason_30010
      @secondary_other_organization = create :organization,
      :searchable,
      primary_social_reason: social_reason_1005,
      secondary_social_reason: social_reason_1005
      Organization.reindex
    end

    scenario "I can see all the corresponding Organizations", vcr: false  do
      visit "/organizations"
      select("9005 - maisons de jeunes, foyers, clubs de jeunes")
      select("9007 - maisons de la culture, office municipal,centres culturels")
      click_on "Search"

      expect(page).to have_content(@matching_organization_1.title)
      expect(page).to have_content(@matching_organization_2.title)
      expect(page).to have_content(@secondary_matching_organization.title)
      expect(page).not_to have_content(@other_organization_1.title)
      expect(page).not_to have_content(@other_organization_2.title)
      expect(page).not_to have_content(@secondary_other_organization.title)

      within "#current_filters" do
        expect(page).to have_content "9005"
        expect(page).to have_content "9007"
      end
    end
  end
end
