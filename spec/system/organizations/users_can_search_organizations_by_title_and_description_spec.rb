# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Organizations::UsersCanSearchOrganizationsByTitleandDescription", type: :system do
  before do
    driven_by(:rack_test)
    create :organization, :searchable, title: "Specific organization"
    create :organization, :searchable, title: "Similar organization"
    create :organization, :searchable, title: "Only all"
    create :organization, :searchable, title: "Specifyk misspelled organysation"
    create :organization, :searchable, title: "Found by reason", reason: "This is a specific organization"
    create :organization, :searchable, title: "Found by misspelled reason", reason: "This is a spasific misspelled organysation"
  end

  context "When I search for all organizations" do
    scenario "I see them all in the results" do
      visit "/organizations"
      fill_in "Search", with: ""
      click_on "Search"

      expect(page).to have_content("Active organizations")

      expect(page).to have_content("Specific organization")
      expect(page).to have_content("Similar organization")
      expect(page).to have_content("Specifyk misspelled organysation")
      expect(page).to have_content("Only all")
      expect(page).to have_content("Found by reason")
      expect(page).to have_content("Found by misspelled reason")
    end
  end

  context "When I search for a specific organization title or reason" do
    scenario "I see the specific organizations in the results" do
      visit "/organizations"
      fill_in "Search", with: "Specific"
      click_on "Search"

      expect(page).to have_content("Results for 'Specific'")

      expect(page).to have_content("Specific organization")
      expect(page).to have_content("Specifyk misspelled organysation")
      expect(page).to have_content("Found by reason")
      expect(page).to have_content("Found by misspelled reason")

      expect(page).not_to have_content("Similar organization")
      expect(page).not_to have_content("Only all")
    end
  end

  context "When I search for similar organization titles or reasons" do
    scenario "I see the similar organizations in the results" do
      visit "/organizations"
      fill_in "Search", with: "organization"
      click_on "Search"

      expect(page).to have_content("Results for 'organization'")

      expect(page).to have_content("Specific organization")
      expect(page).to have_content("Similar organization")
      expect(page).to have_content("Found by reason")
      expect(page).to have_content("Found by misspelled reason")
      expect(page).to have_content("Specifyk misspelled organysation")

      expect(page).not_to have_content("Only all")
    end
  end

  context "When I search for a specific organization by title only" do
    scenario "I only see the specific organizations in the results" do
      visit "/organizations"
      fill_in "Search", with: "Specific"
      check "Search by title only"
      click_on "Search"

      expect(page).to have_content("Results for 'Specific'")

      expect(page).to have_content("Specific organization")
      expect(page).to have_content("Specifyk misspelled organysation")

      expect(page).not_to have_content("Found by reason")
      expect(page).not_to have_content("Similar organization")
      expect(page).not_to have_content("Only all")
      expect(page).not_to have_content("Found by misspelled reason")
    end
  end
end
