# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Organizations::UsersCanFilterOrganizationsByStatuses", type: :system do
  before do
    driven_by(:rack_test)
    @active_organization_1 = create :organization, :active, :searchable
    @active_organization_2 = create :organization, :active, :searchable,
    primary_social_reason: @active_organization_1.primary_social_reason,
    secondary_social_reason: @active_organization_1.primary_social_reason

    @dissolved_organization_1 = create :organization, :dissolved, :searchable,
    primary_social_reason: @active_organization_1.primary_social_reason,
    secondary_social_reason: @active_organization_1.primary_social_reason
    @dissolved_organization_2 = create :organization, :dissolved, :searchable,
    primary_social_reason: @active_organization_1.primary_social_reason,
    secondary_social_reason: @active_organization_1.primary_social_reason

    @canceled_organization_1 = create :organization, :canceled, :searchable,
    primary_social_reason: @active_organization_1.primary_social_reason,
    secondary_social_reason: @active_organization_1.primary_social_reason
    @canceled_organization_2 = create :organization, :canceled, :searchable,
    primary_social_reason: @active_organization_1.primary_social_reason,
    secondary_social_reason: @active_organization_1.primary_social_reason

    Organization.reindex
  end

  context "When I keep the status 'active' checked by default" do
    scenario "I can see only active Organizations and 'active' checked" do
      visit "/organizations"
      expect(page).to have_content @active_organization_1.title
      expect(page).to have_content @active_organization_2.title

      expect(page).not_to have_content @dissolved_organization_1.title
      expect(page).not_to have_content @dissolved_organization_2.title
      expect(page).not_to have_content @canceled_organization_1.title
      expect(page).not_to have_content @canceled_organization_2.title

      expect(page).to have_checked_field "active"
      expect(page).to have_unchecked_field "dissolved"
      expect(page).to have_unchecked_field "canceled"

      within "#current_filters" do
        expect(page).to have_content "active"
        expect(page).not_to have_content "dissolved"
        expect(page).not_to have_content "canceled"
      end
    end
  end

  context "When I check only the status 'active'" do
    scenario "I can see only active Organizations and 'active' checked" do
      visit "/organizations"
      uncheck "active"
      check "active"

      click_on "Search"

      expect(page).to have_content @active_organization_1.title
      expect(page).to have_content @active_organization_2.title

      expect(page).not_to have_content @dissolved_organization_1.title
      expect(page).not_to have_content @dissolved_organization_2.title
      expect(page).not_to have_content @canceled_organization_1.title
      expect(page).not_to have_content @canceled_organization_2.title

      expect(page).to have_checked_field "active"
      expect(page).to have_unchecked_field "dissolved"
      expect(page).to have_unchecked_field "canceled"

      within "#current_filters" do
        expect(page).to have_content "active"
        expect(page).not_to have_content "dissolved"
        expect(page).not_to have_content "canceled"
      end
    end
  end

  context "When I check only the status 'dissolved'" do
    scenario "I can see only dissolved Organizations and 'dissolved' checked" do
      visit "/organizations"
      uncheck "active"
      check "dissolved"
      click_on "Search"

      expect(page).to have_content @dissolved_organization_1.title
      expect(page).to have_content @dissolved_organization_2.title

      expect(page).not_to have_content @active_organization_1.title
      expect(page).not_to have_content @active_organization_2.title
      expect(page).not_to have_content @canceled_organization_1.title
      expect(page).not_to have_content @canceled_organization_2.title

      expect(page).to have_checked_field "dissolved"
      expect(page).to have_unchecked_field "active"
      expect(page).to have_unchecked_field "canceled"

      within "#current_filters" do
        expect(page).to have_content "dissolved"
        expect(page).not_to have_content "active"
        expect(page).not_to have_content "canceled"
      end
    end
  end

  context "When I check only the status 'canceled'" do
    scenario "I can see only canceled Organizations and 'canceled' checked" do
      visit "/organizations"
      uncheck "active"
      check "canceled"
      click_on "Search"

      expect(page).to have_content @canceled_organization_1.title
      expect(page).to have_content @canceled_organization_2.title

      expect(page).not_to have_content @dissolved_organization_1.title
      expect(page).not_to have_content @dissolved_organization_2.title
      expect(page).not_to have_content @active_organization_1.title
      expect(page).not_to have_content @active_organization_2.title

      expect(page).to have_checked_field "canceled"
      expect(page).to have_unchecked_field "active"
      expect(page).to have_unchecked_field "dissolved"

      within "#current_filters" do
        expect(page).to have_content "canceled"
        expect(page).not_to have_content "active"
        expect(page).not_to have_content "dissolved"
      end
    end
  end

  context "When I check all the three statuses 'active', 'dissolved' and 'canceled'" do
    scenario "I can see all Organizations all the three statuses checked" do
      visit "/organizations"
      check "canceled"
      check "dissolved"
      click_on "Search"

      expect(page).to have_content @canceled_organization_1.title
      expect(page).to have_content @canceled_organization_2.title
      expect(page).to have_content @dissolved_organization_1.title
      expect(page).to have_content @dissolved_organization_2.title
      expect(page).to have_content @active_organization_1.title
      expect(page).to have_content @active_organization_2.title

      expect(page).to have_checked_field "active"
      expect(page).to have_checked_field "dissolved"
      expect(page).to have_checked_field "canceled"

      within "#current_filters" do
        expect(page).to have_content "dissolved"
        expect(page).to have_content "active"
        expect(page).to have_content "canceled"
      end
    end
  end
end
