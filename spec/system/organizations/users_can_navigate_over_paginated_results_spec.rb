# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Organizations::UsersCanNavigateOverPaginatedResults", type: :system do
  before do
    driven_by(:rack_test)
  end

  context "with 17 Organizations" do
    before(:each) do
      primary_social_reason = create :social_reason, :with_code_9005
      secondary_social_reason = create :social_reason, :with_code_9007
      create_list(:organization, 17, :searchable,
      primary_social_reason:,
      secondary_social_reason:) do |organization, i|
        organization.title = "Maching organization #{i}"
        organization.save
      end
      Organization.reindex
    end

    context "without Social Reason filter" do
      scenario "When I search for all Organizations I can see the organizations results split on two pages" do
        visit "/organizations"
        fill_in "Search", with: ""
        click_on "Search"

        within ".pagination" do
          within ".page-item.active" do
            expect(page).to have_content("1")
          end

          click_on "2"
          within ".page-item.active" do
            expect(page).to have_content("2")
          end
          expect(page).not_to have_content("3")
        end
      end
    end

    context "with Social Reason filter" do
      scenario "When I search for all Organizations I can see the organizations results split on two pages" do
        visit "/organizations"
        fill_in "Search", with: ""
        select "9005 - maisons de jeunes, foyers, clubs de jeunes"
        click_on "Search"

        within ".pagination" do
          within ".page-item.active" do
            expect(page).to have_content("1")
          end

          click_on "2"
          within ".page-item.active" do
            expect(page).to have_content("2")
          end
          expect(page).not_to have_content("3")
        end
      end
    end
  end


  context "with 5 Organizations" do
    before(:each) do
      create_list :organization, 5, :searchable do |organization, i|
        organization.title = "Maching organization #{i}"
        organization.save
      end
      Organization.reindex
    end

    scenario "When I search for all Organizations I can't see pages navigation" do
      visit "/organizations"
      fill_in "Search", with: ""
      click_on "Search"

      expect(page).not_to have_css(".pagination")
    end
  end
end
