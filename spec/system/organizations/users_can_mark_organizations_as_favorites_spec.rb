# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Organizations::UsersCanMarkOrganizationsAsFavorites", type: :system do
  before do
    driven_by(:selenium)
    @user = create :user
    @organization = create :organization, :searchable
    sign_in @user
  end

  context "As Signed in User" do
    context "When I click on the favorite icon of an Organization" do
      context "for a non-favorite Organization" do
        scenario "I see the Organization marked as favorite" do
          visit "/organizations"

          expect(page).not_to have_content("Remove from favorites")
          click_on "Add to favorites"

          expect(page).to have_content("Remove from favorites")
          expect(page).not_to have_content("Add to favorites")
        end
      end

      context "for a favorite Organization" do
        before do
          @user.favorite @organization
        end

        scenario "I see the Organization marked as favorite" do
          visit "/organizations"

          expect(page).not_to have_content("Add to favorites")

          click_on "Remove from favorites"

          expect(page).to have_content("Add to favorites")
          expect(page).not_to have_content("Remove from favorites")
        end
      end
    end

    context "When I click on the favorite icon multiple times" do
      scenario "I see the favorite button toggleing" do
        visit "/organizations"

        expect(page).not_to have_content("Remove from favorites")

        click_on "Add to favorites"

        expect(page).to have_content("Remove from favorites")
        expect(page).not_to have_content("Add to favorites")

        click_on "Remove from favorites"

        expect(page).to have_content("Add to favorites")
        expect(page).not_to have_content("Remove from favorites")

        click_on "Add to favorites"

        expect(page).to have_content("Remove from favorites")
        expect(page).not_to have_content("Add to favorites")

        click_on "Remove from favorites"

        expect(page).to have_content("Add to favorites")
        expect(page).not_to have_content("Remove from favorites")
      end
    end
  end

  context "As Unsigned User" do
    before do
      sign_out @user
    end

    scenario "I don'tsee any favorite action" do
      visit "/organizations"

      expect(page).not_to have_content("Add to favorites")
      expect(page).not_to have_content("Remove from favorites")
    end
  end
end
