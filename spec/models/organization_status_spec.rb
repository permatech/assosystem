# frozen_string_literal: true

require "rails_helper"

RSpec.describe OrganizationStatus, type: :model do
  subject { build :organization_status }

  it { expect(subject).to belong_to :organization }
  it { expect(subject).to be_active }

  let(:dissolved_status) { build :organization_status, :dissolved }
  it { expect(dissolved_status).to be_dissolved }

  let(:canceled_status) { build :organization_status, :canceled }
  it { expect(canceled_status).to be_canceled }

  describe "original_status" do
    it { expect(subject.original_status).to eq "A" }
    it { expect(dissolved_status.original_status).to eq "D" }
    it { expect(canceled_status.original_status).to eq "S" }
  end
end
