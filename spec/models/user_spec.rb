# frozen_string_literal: true

require "rails_helper"

RSpec.describe User, type: :model do
  subject { build :user }

  it { expect(subject).to be_valid }
  it { expect(subject).to respond_to :all_favorites }
end
