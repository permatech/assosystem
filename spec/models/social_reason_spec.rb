# frozen_string_literal: true

require "rails_helper"

RSpec.describe SocialReason, type: :model do
  subject { build :social_reason }

  it { expect(subject).to belong_to :data_file }
  it { expect(subject).to have_many :primary_organizations }
  it { expect(subject).to have_many :secondary_organizations }

  it { expect(subject).to validate_presence_of :parent_code }
  it { expect(subject).to validate_presence_of :code }
  it { expect(subject).to validate_presence_of :description }
  it { expect(subject).to validate_uniqueness_of(:code).case_insensitive }
end
