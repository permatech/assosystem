# frozen_string_literal: true

require "rails_helper"

RSpec.describe DataFile, type: :model do
  subject { build :data_file }

  it { expect(subject).to have_many :organizations }
  it { expect(subject).to have_many :social_reasons }

  it { expect(subject).to be_valid }
  it { expect(subject).to validate_presence_of :original_filename }
  it { expect(subject).to validate_presence_of :content }
end
