# frozen_string_literal: true

require "rails_helper"

RSpec.describe Favorite, type: :model do
  subject { build :favorite }

  it { expect(subject).to belong_to :favoritable }
  it { expect(subject).to belong_to :favoritor }

  it { expect(subject).to have_rich_text :notes }
end
