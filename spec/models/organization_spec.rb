# frozen_string_literal: true

require "rails_helper"

RSpec.describe Organization, type: :model do
  subject { build :organization }

  it { expect(subject).to belong_to :data_file }
  it { expect(subject).to belong_to :primary_social_reason }
  it { expect(subject).to belong_to(:secondary_social_reason).required false }
  it { expect(subject).to have_one(:administrative_status).dependent :destroy }
  it { expect(subject).to accept_nested_attributes_for(:administrative_status) }

  it { expect(subject).to be_valid }
  it { expect(subject).to validate_presence_of :national_uid }
  it { expect(subject).to validate_presence_of :site_administrator_code }
  it { expect(subject).to validate_presence_of :creation_date }
  it { expect(subject).to validate_presence_of :title }
  it { expect(subject).to validate_presence_of :publication_date }
  it { expect(subject).to validate_presence_of :data_update_time }

  it { expect(subject).to validate_uniqueness_of :national_uid }
  it { expect(subject).to validate_uniqueness_of(:siret).case_insensitive }

  let(:active_organization) { create :organization, :active }
  it { expect(active_organization).to be_active }
  let(:dissolved_organization) { create :organization, :dissolved }
  it { expect(dissolved_organization).to be_dissolved }
  let(:canceled_organization) { create :organization, :canceled }
  it { expect(canceled_organization).to be_canceled }

  it { expect(subject).to respond_to :favoritors }
end
