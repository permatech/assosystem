# frozen_string_literal: true

# Prevent Rspec to raise error when console tag is present in views
def console
  view_path = "app/views/#{controller_name}/#{action_name}.html.erb"
  p "WARNING <%= console %> tag is rendered within '#{view_path}'"
  true
end
