# frozen_string_literal: true

require "rails_helper"

RSpec.describe OrganizationHelper, type: :helper do
  before do
    @active_organization = create :organization
    @dissolved_organization = create :organization, :dissolved
    @canceledd_organization = create :organization, :canceled
  end

  describe "h_status_color" do
    it { expect(helper.h_status_color(@active_organization.administrative_status.status)).to eq "success" }
    it { expect(helper.h_status_color(@dissolved_organization.administrative_status.status)).to eq "warning" }
    it { expect(helper.h_status_color(@canceledd_organization.administrative_status.status)).to eq "danger" }
  end
end
