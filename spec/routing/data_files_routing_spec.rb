# frozen_string_literal: true

require "rails_helper"

def authenticate!(any)
  true
end

public def user(any)
  FactoryBot.create :user, :admin
end

RSpec.describe DataFilesController, type: :routing do
  describe "routing" do
    describe "permission" do
      it { expect(get: "/data_files").to route_to("data_files#index") }
      it { expect(get: "/data_files/new").to route_to("data_files#new") }
      it { expect(post: "/data_files").to route_to("data_files#create") }
    end

    describe "restriction" do
      it { expect(get: "/data_files/1").not_to be_routable }
      it { expect(get: "/data_files/1/edit").not_to be_routable }
      it { expect(put: "/data_files/1").not_to be_routable }
      it { expect(patch: "/data_files/1").not_to be_routable }
      it { expect(delete: "/data_files/1").not_to be_routable }
    end
  end
end
