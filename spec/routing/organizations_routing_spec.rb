# frozen_string_literal: true

require "rails_helper"

RSpec.describe OrganizationsController, type: :routing do
  describe "routing" do
    describe "permission" do
      it { expect(get: "/organizations").to route_to "organizations#index" }
      it { expect(get: "/organizations/1").to route_to "organizations#show", id: "1" }
    end

    describe "restriction" do
      it { expect(get: "/organizations/new").not_to route_to "organizations#new" }
      it { expect(get: "/organizations/1/edit").not_to be_routable }
      it { expect(post: "/organizations").not_to be_routable }
      it { expect(put: "/organizations/1").not_to be_routable }
      it { expect(patch: "/organizations/1").not_to be_routable }
      it { expect(delete: "/organizations/1").not_to be_routable }
    end
  end
end
