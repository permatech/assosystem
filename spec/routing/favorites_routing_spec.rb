# frozen_string_literal: true

require "rails_helper"

RSpec.describe FavoritesController, type: :routing do
  describe "routing" do
    describe "permission" do
     it { expect(post: "/favorites").to route_to "favorites#create" }
     it { expect(delete: "/favorites/1").to route_to "favorites#destroy", id: "1" }
     it { expect(get: "/favorites/1/edit").to route_to "favorites#edit", id: "1" }
     it { expect(put: "/favorites/1").to route_to "favorites#update", id: "1" }
     it { expect(patch: "/favorites/1").to route_to "favorites#update", id: "1" }
   end

    describe "restriction" do
      it { expect(get: "/favorites").not_to be_routable }
      it { expect(get: "/favorites/1").not_to be_routable }
      it { expect(get: "/favorites/new").not_to route_to "favorites#new" }
    end
  end
end
