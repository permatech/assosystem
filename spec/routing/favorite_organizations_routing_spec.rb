# frozen_string_literal: true

require "rails_helper"

RSpec.describe FavoriteOrganizationsController, type: :routing do
  describe "routing" do
    describe "permission" do
       it { expect(get: "/favorite_organizations").to route_to "favorite_organizations#index" }
       it { expect(get: "/favorite_organizations/1").to route_to "favorite_organizations#show", id: "1" }
     end

    describe "restriction" do
     it { expect(post: "/favorite_organizations").not_to be_routable }
     it { expect(get: "/favorite_organizations/new").not_to route_to "favorite_organizations#new" }
     it { expect(delete: "/favorite_organizations/1").not_to be_routable }
     it { expect(get: "/favorite_organizations/1/edit").not_to be_routable }
     it { expect(put: "/favorite_organizations/1").not_to be_routable }
     it { expect(patch: "/favorite_organizations/1").not_to be_routable }
   end
  end
end
