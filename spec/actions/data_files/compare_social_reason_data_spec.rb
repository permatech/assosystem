# frozen_string_literal: true

require "rails_helper"

RSpec.describe DataFiles::CompareSocialReasonData, type: :action do
  context "With an unchanged an edited and a new Social Reason" do
    before do
      create :social_reason, :with_code_9000
      create :social_reason, :with_code_9005
    end

    let(:valid_context) do
      LightService::Testing::ContextFactory
      .make_from(DataFiles::ImportResourcesFromCsvFile)
      .for(described_class)
      .with({
        data_category: "1",
        resources_data: fixture_file_upload("spec/support/test_data_files/fake_social_reasons_comparison.csv")
      })
    end

    subject { described_class.execute(valid_context) }

    it { expect(subject.extracted_data.count).to eq(3) }
    it { expect(subject.new_social_reasons_data.count).to eq(1) }
    it { expect(subject.edited_social_reasons_data.count).to eq(1) }
  end
end
