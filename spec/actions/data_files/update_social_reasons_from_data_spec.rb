# frozen_string_literal: true

require "rails_helper"

RSpec.describe DataFiles::UpdateSocialReasonsFromData, type: :action do
  context "with edited Social Reasons" do
    before do
      @unchanged_social_reason = create :social_reason, :with_code_9000
      @edited_social_reason = create :social_reason, :with_code_9005
    end

    let(:context) do
      LightService::Testing::ContextFactory
      .make_from(DataFiles::ImportResourcesFromCsvFile)
      .for(described_class)
      .with({
        data_category: "1",
        resources_data: fixture_file_upload("spec/support/test_data_files/fake_social_reasons_comparison.csv")
      })
    end

    subject { described_class.execute(context) }

    it { expect(subject).to be_a_success }

    it { expect { subject }.not_to change(@unchanged_social_reason, :updated_at) }
    it { expect {
      subject
      @edited_social_reason.reload
    }.to change(@edited_social_reason, :updated_at) }
  end
end
