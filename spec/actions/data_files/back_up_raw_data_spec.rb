# frozen_string_literal: true

require "rails_helper"

RSpec.describe DataFiles::BackUpRawData, type: :action do
  context "for Organizations" do
    let(:valid_context) do
      LightService::Testing::ContextFactory
      .make_from(DataFiles::ImportResourcesFromCsvFile)
      .for(described_class)
      .with({
        data_category: "0",
        resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_2_rows.csv")
      })
    end

    subject { described_class.execute(valid_context) }

    it { expect(subject).to be_a_success }
    it { expect { subject }.to change(DataFile, :count).by 1 }
    it { expect(subject.data_file_id).to eq DataFile.last.id  }
  end

  context "for Social Reasons" do
    let(:valid_context) do
      LightService::Testing::ContextFactory
      .make_from(DataFiles::ImportResourcesFromCsvFile)
      .for(described_class)
      .with({
        data_category: "1",
        resources_data: fixture_file_upload("spec/support/test_data_files/fake_social_reasons_30_rows.csv")
      })
    end

    subject { described_class.execute(valid_context) }

    it { expect(subject).to be_a_success }
    it { expect { subject }.to change(DataFile, :count).by 1 }
    it { expect(subject.data_file_id).to eq DataFile.last.id  }
  end
end
