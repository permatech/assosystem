# frozen_string_literal: true

require "rails_helper"

RSpec.describe DataFiles::CreateOrganizationsFromData, type: :action do
  let(:context) do
    LightService::Testing::ContextFactory
    .make_from(DataFiles::ImportResourcesFromCsvFile)
    .for(described_class)
    .with({
      data_category: "0",
      resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_comparison.csv")
    })
  end

  subject { described_class.execute(context) }

  context "When no Organizations exist" do
    before do
      @social_reason_1 = create :social_reason, :for_rna_row_1
      @social_reason_2 = create :social_reason, :for_rna_row_2
    end
    it { expect(subject).to be_a_success }
    it { expect { subject }.to change(Organization, :count).by(3) }
    it { expect { subject }.to change(OrganizationStatus, :count).by(3) }
    it "associates Social Reasons to Organizations" do
      subject
      expect(@social_reason_1.reload.primary_organization_ids.count).to eq(2)
      expect(@social_reason_2.reload.primary_organization_ids.count).to eq(1)
    end
  end

  context "when some Organizations already exist" do
    before do
      create :social_reason, :for_rna_row_1
      create :social_reason, :for_rna_row_2
      @unchanged_organization = create :organization, national_uid: "W9M1000001", data_update_time: "2008-08-11 23:43:32"
      @edited_organization = create :organization, national_uid: "W9M1000002", data_update_time: "2007-04-04 18:40:20"
    end

    it { expect(subject).to be_a_success }

    it { expect { subject }.not_to change(@unchanged_organization, :updated_at) }
    it { expect { subject }.to change(Organization, :count).by(1) }
    it { expect(subject.message).to be_nil }
  end
end
