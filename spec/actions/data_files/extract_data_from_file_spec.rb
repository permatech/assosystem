# frozen_string_literal: true

require "rails_helper"

RSpec.describe DataFiles::ExtractDataFromFile, type: :action do
  context "for Organizations" do
    context "with well formed CSV file" do
      let(:context) do
        {
          data_category: "0",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_2_rows.csv")
        }
      end

      subject { described_class.execute(context) }

      it { expect(subject).to be_a_success }

      it { expect(subject.extracted_data).to be_a Array }
      it { expect(subject.extracted_data.first).to be_a Hash }
      it { expect(subject.extracted_data.count).to eq 2 }
    end

    context "with mal-formed CSV file" do
      let(:failing_context) do
        {
          data_category: "0",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_bad.csv")
        }
      end

      subject { described_class.execute(failing_context) }

      it { expect(subject).to be_a_failure }
      it { expect(subject.extracted_data).to eq nil }
      it { expect(subject.message).to include "Illegal quoting in line 1." }
    end
  end

  context "for Social Reasons" do
    context "with well formed CSV file" do
      let(:context) do
        {
          data_category: "1",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_social_reasons_30_rows.csv")
        }
      end

      subject { described_class.execute(context) }

      it { expect(subject).to be_a_success }
      it { expect(subject.extracted_data).to be_a Array }
      it { expect(subject.extracted_data.count).to eq 30 }
      it { expect(subject.extracted_data.first).to be_a Hash }
      it { expect(subject.extracted_data).not_to be_empty }
    end

    context "with mal-formed CSV file" do
      let(:failing_context) do
        {
          data_category: "1",
          resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_bad.csv")
        }
      end

      subject { described_class.execute(failing_context) }

      it { expect(subject).to be_a_failure }
      it { expect(subject.extracted_data).to eq nil }
      it { expect(subject.message).to include "Illegal quoting in line 1." }
    end
  end
end
