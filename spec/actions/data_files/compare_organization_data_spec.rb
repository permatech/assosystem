# frozen_string_literal: true

require "rails_helper"

RSpec.describe DataFiles::CompareOrganizationData, type: :action do
  context "With an unchanged, an edited and a new Organizations" do
    before do
      create :social_reason, :for_rna_row_1
      create :social_reason, :for_rna_row_2
      create :organization, national_uid: "W9M1000001", data_update_time: "2008-08-11 23:43:32"
      create :organization, national_uid: "W9M1000002", data_update_time: "2007-04-02 18:40:20"
    end

    let(:valid_context) do
      LightService::Testing::ContextFactory
      .make_from(DataFiles::ImportResourcesFromCsvFile)
      .for(described_class)
      .with({
        data_category: "0",
        resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_comparison.csv")
      })
    end

    subject { described_class.execute(valid_context) }

    it { expect(subject.extracted_data.count).to eq(3) }
    it { expect(subject.new_organizations_data.count).to eq(1) }
    it { expect(subject.edited_organizations_data.count).to eq(1) }
  end

  context "With only unchanged Organizations" do
    before do
      create :social_reason, :for_rna_row_1
      create :social_reason, :for_rna_row_2
      create :organization, national_uid: "W9M1000001", data_update_time: "2008-08-11 23:43:32"
      create :organization, national_uid: "W9M1000002", data_update_time: "2007-04-03 18:40:20"
    end

    let(:valid_context) do
      LightService::Testing::ContextFactory
      .make_from(DataFiles::ImportResourcesFromCsvFile)
      .for(described_class)
      .with({
        data_category: "0",
        resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_2_rows.csv")
      })
    end

    subject { described_class.execute(valid_context) }

    it { expect { subject }.to raise_error LightService::FailWithRollbackError }
  end
end
