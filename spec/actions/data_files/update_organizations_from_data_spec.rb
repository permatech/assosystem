# frozen_string_literal: true

require "rails_helper"

RSpec.describe DataFiles::UpdateOrganizationsFromData, type: :action do
  context "with edited Organizations" do
    before do
      create :social_reason, :for_rna_row_1
      create :social_reason, :for_rna_row_2
      @unchanged_organization = create :organization, national_uid: "W9M1000001", data_update_time: "2008-08-11 23:43:32"
      @edited_organization = create :organization, national_uid: "W9M1000002", data_update_time: "2007-04-02 18:40:20"
    end

    let(:context) do
      LightService::Testing::ContextFactory
      .make_from(DataFiles::ImportResourcesFromCsvFile)
      .for(described_class)
      .with({
        data_category: "0",
        resources_data: fixture_file_upload("spec/support/test_data_files/fake_rna_comparison.csv")
      })
    end

    subject { described_class.execute(context) }

    it { expect(subject).to be_a_success }

    it { expect { subject }.not_to change(@unchanged_organization, :updated_at) }
    it { expect {
      subject
      @edited_organization.reload
    }.to change(@edited_organization, :updated_at) }
  end
end
