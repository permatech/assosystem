# frozen_string_literal: true

module DataFiles
  class UpdateSocialReasonsFromData
    extend ::LightService::Action

    expects  :data_file_id, :edited_social_reasons_data
    promises :resources_errors

    executed do |context|
      data_file = DataFile.find(context.data_file_id)
      context.edited_social_reasons_data.each do |data|
        resource = SocialReason.find_by(code: data[:code])
        resource.update data
        resource.data_file = data_file

        unless resource.save
          context.resources_errors[:"#{resource.code}"] = resource.errors
        end
      end
      SocialReason.search_index.refresh
    end

    rolled_back do |context|
      data_file = DataFile.find(context.data_file_id)
      data_file.social_reasons.each do |social_reason|
        social_reason.destroy
      end
    end
  end
end
