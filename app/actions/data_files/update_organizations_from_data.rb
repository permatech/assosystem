# frozen_string_literal: true

module DataFiles
  class UpdateOrganizationsFromData
    extend ::LightService::Action

    expects  :data_file_id, :edited_organizations_data
    promises :resources_errors

    executed do |context|
      data_file = DataFile.find(context.data_file_id)
      context.edited_organizations_data.each do |data|
        resource = Organization.find_by(national_uid: data[:national_uid])
        resource.update data
        resource.data_file = data_file

        unless resource.primary_social_reason.code == data[:social_reason_1]
          resource.primary_social_reason = SocialReason.find_by(code: data[:social_reason_1])
        end
        unless resource.secondary_social_reason.code == data[:social_reason_2]
          resource.secondary_social_reason = SocialReason.find_by(code: data[:social_reason_2])
        end
        unless resource.administrative_status.original_status == data[:status]
          resource.administrative_status.update_attribute(status: OrganizationStatus.status_mappings[data[:status]])
        end

        unless resource.save
          context.resources_errors[:"#{resource.national_uid}"] = resource.errors
        end
      end
      Organization.search_index.refresh
    end

    rolled_back do |context|
      data_file = DataFile.find(context.data_file_id)
      data_file.organizations.each do |organization|
        organization.destroy
      end
    end
  end
end
