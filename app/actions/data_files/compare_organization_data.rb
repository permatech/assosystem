# frozen_string_literal: true

module DataFiles
  class CompareOrganizationData
    extend ::LightService::Action

    expects :extracted_data
    promises :new_organizations_data, :edited_organizations_data

    executed do |context|
      new_organizations_data = []
      edited_organizations_data = []

      context.extracted_data.each do |organization_data|
        organization = Organization.find_by(national_uid: organization_data[:national_uid])

        if organization.present?
          if organization_data[:data_update_time] > organization.data_update_time
            edited_organizations_data << organization_data
          end
        else
          new_organizations_data << organization_data
        end
      end

      if new_organizations_data.empty? && edited_organizations_data.empty?
        context.fail_with_rollback! "No Organizations were created nor updated."
      end

      context.new_organizations_data = new_organizations_data
      context.edited_organizations_data = edited_organizations_data
    end

    rolled_back do |context|
      # FIXME: rollback not propagated. The DataFiles::BackUpRawData roolback has been duplicated here.
      DataFile.find(
        context.data_file_id
      ).destroy
      context.data_file_id = nil
    end
  end
end
