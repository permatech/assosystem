# frozen_string_literal: true

module DataFiles
  class CompareSocialReasonData
    extend ::LightService::Action

    expects :extracted_data
    promises :new_social_reasons_data, :edited_social_reasons_data

    executed do |context|
      new_social_reasons_data = []
      edited_social_reasons_data = []

      context.extracted_data.each do |social_reason_data|
        social_reason = SocialReason.find_by(code: social_reason_data[:code])

        if social_reason.present?
          unless social_reason_data[:description] == social_reason.description
            edited_social_reasons_data << social_reason_data
          end
        else
          new_social_reasons_data << social_reason_data
        end
      end

      if new_social_reasons_data.empty? && edited_social_reasons_data.empty?
        context.fail_with_rollback! "No Social Reasons were created nor updated."
      end

      context.new_social_reasons_data = new_social_reasons_data
      context.edited_social_reasons_data = edited_social_reasons_data
    end

    rolled_back do |context|
      # FIXME: rollback not propagated. The DataFiles::BackUpRawData roolback has been duplicated here.
      DataFile.find(
        context.data_file_id
      ).destroy
      context.data_file_id = nil
    end
  end
end
