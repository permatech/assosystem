# frozen_string_literal: true

module DataFiles
  class ExtractDataFromFile
    extend ::LightService::Action

    expects :data_category, :resources_data
    promises :extracted_data, :resource_class

    executed do |context|
      resource_class = DataFile.data_categories.key(context.data_category).to_s.classify.constantize
      options = {
        force_utf8: resource_class.csv_options[:force_utf8],
        col_sep: resource_class.csv_options[:col_sep],
        header_transformations: [key_mapping: resource_class.csv_options[:headers_mapping]]
      }

      context.extracted_data = SmarterCSV.process(context.resources_data.path, options)
      context.resource_class = resource_class
    rescue CSV::MalformedCSVError => error
      context.fail! error.message
    end
  end
end
