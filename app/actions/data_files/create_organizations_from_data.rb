# frozen_string_literal: true

module DataFiles
  class CreateOrganizationsFromData
    extend ::LightService::Action

    expects  :data_file_id, :new_organizations_data
    promises :resources_errors

    executed do |context|
      data_file = DataFile.find(context.data_file_id)

      context.new_organizations_data.each do |data|
        resource = Organization.new(data)
        resource.data_file = data_file

        resource.primary_social_reason = SocialReason.find_by(code: data[:social_reason_1])
        resource.secondary_social_reason = SocialReason.find_by(code: data[:social_reason_2]) || nil


        OrganizationStatus.status_mappings.each do |key, value|
          resource.build_administrative_status(status: value) if data[:status] == key.to_s
        end

        context.resources_errors = {}
        resource.save
        unless resource.persisted?
          context.resources_errors[:"#{resource.national_uid}"] = resource.errors
          context.message = context.resources_errors
        end
      end
      Organization.search_index.refresh
    end

    rolled_back do |context|
      data_file = DataFile.find(context.data_file_id)
      data_file.organizations.each do |organization|
        organization.destroy
      end
    end
  end
end
