# frozen_string_literal: true

module DataFiles
  class BackUpRawData
    extend ::LightService::Action

    expects :resources_data
    promises :data_file_id

    executed do |context|
      resources_data = context.resources_data

      normalized_content = resources_data.tempfile.open.read
      unless normalized_content.valid_encoding?
        normalized_content.encode!(Encoding::UTF_8, invalid: :replace, undef: :replace)
      end

      data_file = DataFile.create(
        original_filename: resources_data.original_filename,
        content: normalized_content
      )

      if data_file.errors.any?
        context.fail_with_rollback! data_file.errors.full_messages.split(", ")
      end
      context.data_file_id = data_file.id
    end

    rolled_back do |context|
      # FIXME: rollback not triggered from DataFiles::CompareOrganizationData and DataFiles::CompareSocialReasonData.
      DataFile.find(
        context.data_file_id
      ).destroy
      context.data_file_id = nil
    end
  end
end
