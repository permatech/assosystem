# frozen_string_literal: true

module DataFiles
  class CreateSocialReasonsFromData
    extend ::LightService::Action

    expects  :data_file_id, :new_social_reasons_data
    promises :resources_errors

    executed do |context|
      data_file = DataFile.find(context.data_file_id)

      context.new_social_reasons_data.each do |data|
        resource = SocialReason.new(data)
        resource.data_file = data_file

        resource.parent_code = resource.parent_code.to_i.to_s
        resource.code = resource.code.to_i.to_s

        context.resources_errors = {}
        unless resource.save
          context.resources_errors[:"#{resource.code}"] = resource.errors
        end
      end
      SocialReason.search_index.refresh
    end

    rolled_back do |context|
      data_file = DataFile.find(context.data_file_id)
      data_file.social_reasons.each do |social_reason|
        social_reason.destroy
      end
    end
  end
end
