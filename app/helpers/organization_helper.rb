# frozen_string_literal: true

module OrganizationHelper
  def h_status_color(status)
    status_colors = {
        active: "success",
        dissolved: "warning",
        canceled: "danger"
    }
    status_colors[status.to_sym]
  end
end
