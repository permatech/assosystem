# frozen_string_literal: true

class FavoritesController < ApplicationController
  before_action :set_favoritor, only: %i(create destroy)
  before_action :set_favoritable, only: %i(create destroy)

  def create
    @favoritor.favorite @favoritable
    @favorite = @favoritor.get_favorite @favoritable

    render turbo_stream: turbo_stream.replace(
      "favoritable_#{@favoritable.class.name.downcase.underscore}_#{@favoritable.id}",
      partial: "unfavorite_form",
      locals: { favoritor: @favoritor, favoritable: @favoritable }
    )
  end

  def edit
    @favorite = Favorite.find params[:id]
  end

  def update
    @favorite = Favorite.find params[:id]
    if @favorite.update favorite_params
      redirect_to favorite_organization_path(@favorite.favoritable)
    end
  end

  def destroy
    @favorite = Favorite.find(params[:id]).destroy

    favorite_organizations = Organization.favorited_by_favoritor current_user
    @favorite_organizations_pagination, @favorite_organizations = pagy favorite_organizations

    render turbo_stream: [
      turbo_stream.replace(
      "favoritable_#{@favoritable.class.name.downcase.underscore}_#{@favoritable.id}",
      partial: "favorite_form",
      locals: { favoritor: @favoritor, favoritable: @favoritable }
    ),
    turbo_stream.update(
      "favorite_organizations",
      partial: "favorite_organizations/favorite_organizations",
      locals: {
        favorite_organizations: @favorite_organizations,
        pagination: @favorite_organizations_pagination
      })
    ]
  end

  private
    def set_favoritor
      @favoritor = User.find favorite_params[:favoritor_id]
    end

    def set_favoritable
      favoritable_class = favorite_params[:favoritable_type].classify.constantize
      @favoritable = favoritable_class.find favorite_params[:favoritable_id]
    end

    def favorite_params
      params.require(:favorite)
      .permit(
        :id,
        :favoritor_id,
        :favoritable_id,
        :favoritable_type,
        :notes
      )
    end
end
