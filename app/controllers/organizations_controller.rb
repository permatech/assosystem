# frozen_string_literal: true

class OrganizationsController < ApplicationController
  DEFAULT_SEARCH = "*"
  ITEMS_SIZE = 10

  def index
    search = params[:search].try(:presence) || DEFAULT_SEARCH

    administrative_statuses = []
    OrganizationStatus.statuses.keys.each do |status_key|
      if params[status_key].present?
        administrative_statuses << status_key
      end
    end
    administrative_statuses << "active" if administrative_statuses.empty?


    if params[:social_reason_ids].present? && params[:social_reason_ids].reject(&:blank?).any?
      social_reason_ids = [params[:social_reason_ids].presence].flatten.reject(&:empty?).map(&:to_s)
      social_reason_filters = true
    else
      social_reason_ids = SocialReason.all.map(&:id)
      social_reason_filters = false
    end

    fields = params[:search_by_title_only].present? ? %i(title) : %i(title reason)

    organizations = Organization.pagy_search(
      search,
      fields:,
      misspellings: { edit_distance: 2 },
      where: {
        social_reason_ids:,
        status: administrative_statuses
      }
    )

    @organizations_pagination, @organizations = pagy_searchkick(organizations, items: ITEMS_SIZE)
    @social_reasons = SocialReason.asc_codes
    filters = {}
    filters[:social_reasons_codes] = SocialReason.find(social_reason_ids).map(&:code) if social_reason_filters
    filters[:statuses] = administrative_statuses
    @filters = filters
  end

  def show
    @organization = Organization.find(params[:id])
  end
end
