# frozen_string_literal: true

class FavoriteOrganizationsController < ApplicationController
  include Pagy::Backend

  before_action :authenticate_user!
  # before_action :set_favorite_organization, only: %i(show edit update)
  # before_action :set_favorite, only: %i(show edit update)

  def index
    favorite_organizations = Organization.favorited_by_favoritor current_user
    @favorite_organizations_pagination, @favorite_organizations = pagy favorite_organizations
  end

  def show
    @favorite_organization = Organization.find params[:id]
    @favorite = @favorite_organization.favorited.where(favoritor: current_user).first
  end

  # def edit
  # end

  # def update
  #   if @favorite.update favorite_params
  #     redirect_to @favoritable_organization
  #   end
  # end

  private
    # def set_favorite
    # end

    # def set_favorite_organization
    # end

    def favoritable_organization_params
      params.require(:favorite_params).permit(:notes)
    end
end
