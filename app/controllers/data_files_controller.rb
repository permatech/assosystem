# frozen_string_literal: true

class DataFilesController < ApplicationController
  before_action :authenticate_user!

  def index
    @data_files = DataFile.all
    @data_file = DataFile.new
  end

  def new
    @data_file = DataFile.new
  end

  def create
    import = DataFiles::ImportResourcesFromCsvFile.call data_file_params

    respond_to do |format|
      if import.success?
        import.resource_class.reindex
        @data_file = DataFile.find import.data_file_id
        format.html {
          redirect_to data_files_path,
          notice: "#{import.resource_class.model_name.human.pluralize} were successfully created."
        }
      else
        @data_file = DataFile.new
        # @data_file.errors.messages << import.message
        format.turbo_stream do
          render turbo_stream: turbo_stream.replace(
            "#{helpers.dom_id(@data_file)}_form",
            partial: "form",
            locals: { data_file: @data_file },
            data: { turbo_frame: "_top" }
          )
        end
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  private
    def data_file_params
      params.require(:data_file).permit(:data_category, :resources_data)
    end
end
