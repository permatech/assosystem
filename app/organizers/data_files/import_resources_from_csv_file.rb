# frozen_string_literal: true

module DataFiles
  class ImportResourcesFromCsvFile
    extend ::LightService::Organizer

    def self.call(params)
      with(
        data_category: params[:data_category],
        resources_data: params[:resources_data]
      ).reduce(actions)
    end

    def self.actions
      [
        DataFiles::ExtractDataFromFile,
        DataFiles::BackUpRawData,
        reduce_if(-> (context) { context.resource_class == Organization },
        [
          DataFiles::CompareOrganizationData,
          DataFiles::CreateOrganizationsFromData,
          DataFiles::UpdateOrganizationsFromData
        ]),
        reduce_if(-> (context) { context.resource_class == SocialReason },
      [
        DataFiles::CompareSocialReasonData,
        DataFiles::CreateSocialReasonsFromData,
        DataFiles::UpdateSocialReasonsFromData
      ])
      ]
    end
  end
end
