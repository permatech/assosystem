# frozen_string_literal: true

class OrganizationStatus < ApplicationRecord
  searchkick

  belongs_to :organization, inverse_of: :administrative_status

  enum status: { active: 0, dissolved: 1, canceled: 2 }

  STATUS_MAPPINGS = {
    "A": "active",
    "D": "dissolved",
    "S": "canceled"
  }.freeze

  def self.status_mappings
    STATUS_MAPPINGS
  end

  def original_status
    STATUS_MAPPINGS.key(status).to_s
  end
end
