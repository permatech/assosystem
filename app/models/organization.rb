# frozen_string_literal: true

class Organization < ApplicationRecord
  extend Pagy::Searchkick
  searchkick word_start: %i(title reason)

  acts_as_favoritable

  belongs_to :data_file
  belongs_to :primary_social_reason, class_name: "SocialReason", foreign_key: "primary_social_reason_id"
  belongs_to :secondary_social_reason, class_name: "SocialReason", foreign_key: "secondary_social_reason_id", required: false
  has_one :administrative_status, class_name: "OrganizationStatus", inverse_of: :organization, dependent: :destroy
  accepts_nested_attributes_for :administrative_status

  validates :national_uid, presence: true, uniqueness: true
  validates_uniqueness_of :siret, allow_blank: true, case_sensitive: false
  validates_presence_of :site_administrator_code,
  :creation_date,
  :title,
  :publication_date,
  :data_update_time

  scope :favorited_by_favoritor, -> (favoritor) { joins(:favorited)
    .where(favorited: { favoritor_type: favoritor.class.name })
    .where(favorited: { favoritor_id: favoritor.id })
  }

  CSV_OPTIONS = {
    force_utf8: true,
    col_sep: ";",
    headers_mapping: {
      id: :national_uid,
      id_ex: :former_uid,
      siret: :siret,
      rup_mi: nil,
      gestion: :site_administrator_code,
      date_creat: :creation_date,
      date_decla: :declaration_date,
      date_publi: :publication_date,
      date_disso: :dissolution_date,
      nature: :kind_code,
      groupement: :grouping,
      titre: :title,
      titre_court: :short_title,
      objet: :reason,
      objet_social1: :social_reason_1,
      objet_social2: :social_reason_2,
      adrs_complement: :additional_address,
      adrs_numvoie: :street_number,
      adrs_repetition: :additional_street_number,
      adrs_typevoie: :street_type,
      adrs_libvoie: :street_name,
      adrs_distrib: :additional_street_name,
      adrs_codeinsee: :insee_address_code,
      adrs_codepostal: :zipcode,
      adrs_libcommune: :municipality_description,
      adrg_declarant: :declarant_family_name,
      adrg_complemid: :manager_address,
      adrg_complemgeo: :manager_additional_address,
      adrg_libvoie: :manager_street_name,
      adrg_distrib: :manager_additional_street_name,
      adrg_codepostal: :manager_zipcode,
      adrg_achemine: :manager_billing_address,
      adrg_pays: :manager_state,
      dir_civilite: :manager_gender,
      siteweb: :web_site,
      publiweb: :web_publication,
      observation: :remark,
      position: :status,
      maj_time: :data_update_time
    }
  }.freeze

  def active?
    administrative_status.active?
  end

  def dissolved?
    administrative_status.dissolved?
  end

  def canceled?
    administrative_status.canceled?
  end

  def search_data
    {
      title:,
      reason:,
      status: administrative_status.status,
      social_reason_ids: [primary_social_reason_id, secondary_social_reason_id]

    }
  end

  def favorite_for_favoritor(favoritor)
    favorited.where(favoritor:).first
  end

  private
    def self.csv_options
      CSV_OPTIONS
    end
end
