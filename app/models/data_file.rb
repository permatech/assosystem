# frozen_string_literal: true

class DataFile < ApplicationRecord
  has_many :organizations
  has_many :social_reasons

  enum :data_category, { organization: "0", social_reason: "1" }

  validates_presence_of :original_filename, :content
end
