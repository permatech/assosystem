# frozen_string_literal: true

class SocialReason < ApplicationRecord
  searchkick

  belongs_to :data_file
  has_many :primary_organizations, class_name: "Organization", foreign_key: "primary_social_reason_id"
  has_many :secondary_organizations, class_name: "Organization", foreign_key: "secondary_social_reason_id"

  validates_presence_of :parent_code, :description
  validates :code, presence: true, uniqueness: true

  scope :asc_codes, -> { order(code: :asc) }

  CSV_OPTIONS = {
    force_utf8: true,
    col_sep: ",",
    headers_mapping: {
        objet_social_parent_id: :parent_code,
        objet_social_id: :code,
        objet_social_lib: :description
    }
  }.freeze

  def organizations
    primary_organizations + secondary_organizations
  end

  def code_with_description_label
    "#{code} - #{description}"
  end

  private
    def self.csv_options
      CSV_OPTIONS
    end
end
